
    let otroArreglo = [];

    document.getElementById('btnGenerar').addEventListener('click', mostrar);
    document.getElementById('btnBorrar').addEventListener('click', limpiar);

    function llenarArray(tamanio) {
        otroArreglo = [];
        for (let i = 0; i < tamanio; ++i) {
            otroArreglo.push(Math.floor(Math.random() * 100));
        }
    }

    function limpiar() {
        document.getElementById("idLista").innerHTML = "";
        document.getElementById("txtPares").value = "";
        document.getElementById("txtImpares").value = "";
        document.getElementById("txtSimetrico").value = "";
    }

    function mostrar() {
        const tamanio = parseInt(document.getElementById('txtCantidad').value);
        if (isNaN(tamanio) || tamanio <= 0) {
            alert("Por favor, ingrese un número válido para el tamaño del arreglo.");
            return;
        }
        llenarArray(tamanio);
        actualizar(otroArreglo);
        document.getElementById("txtPares").value = numPares(otroArreglo);
        document.getElementById("txtImpares").value = numImpares(otroArreglo);
        document.getElementById("txtSimetrico").value = Simetrico(otroArreglo);
    }

    function actualizar(arreglo) {
        const lista = document.getElementById("idLista");
        lista.innerHTML = "";
        arreglo.forEach((valor) => {
            let opcion = new Option(valor, valor);
            lista.add(opcion);
        });
    }

    function numPares(otroArray) {
        return otroArray.filter(x => x % 2 === 0).length;
    }

    function numImpares(otroArray) {
        return otroArray.filter(x => x % 2 !== 0).length;
    }

    function Simetrico(otroArray) {
        let numPares = otroArray.filter(x => x % 2 === 0).length;
        let numImpares = otroArray.filter(x => x % 2 !== 0).length;
        return numPares === numImpares ? "El arreglo es simétrico." : "El arreglo es asimétrico.";
    }
